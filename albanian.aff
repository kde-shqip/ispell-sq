#    albanian.aff
#    Affix table for albanian
#
#    AUTHOR:  Luan Kelmendi <l.k@apolonix.com>
#    VERSION: 1.1 (Aug. 2000)
# 
#    This affix file is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This affix file is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#

#nroffchars	().\\*
texchars	()\[]{}<\>\\$*.%

#compoundmin 8

compoundwords on
 
allaffixes off


defstringtype "list" "nroff" ".list"

boundarychars   [.]
boundarychars	[---] 
boundarychars 	'
wordchars	[a-c]	[A-C]
stringchar	�	�
wordchars	[d-e]	[D-E]
stringchar	�	�
wordchars	[f-z]	[F-Z]

#
# TeX
#
altstringtype "tex" "TeX" ".tex" ".bib"

altstringchar	"\\c c"	�
altstringchar	"\\c C"	�
altstringchar	\\"e	�
altstringchar	\\"E	�


# Here's a record of flags used, in case you want to add new ones.
# Right now, we fit within the minimal MASKBITS definition.
#
#            ABCDEFGHIJKLMNOPQRSTUVWXYZ
# Used:      ********  *** *******  * *
#            


# Now the prefix table.  There are only three prefixes that are truly
# frequent in English, and none of them seem to need conditional variations.
#
prefixes

flag *A:
    .		>	PA		#  barabart� > pabarabart� (1.97%)

flag *E:
    .		>	P�R		#  vjetor > p�rvjetor (1.61%)

#flag *I: 
#    .		>	PARA		#  caktuar > paracaktuar (0.68%)


#flag *O:
#    .		>	MOS		#  besim > mosbesim

#flag *U:
#    .		>	RI		#  shqyrtoj > rishqyrtoj (0.62%)

#flag *Y:
#    .		>	N�N


flag *X: 
    .		>	S'		#  �sht� > s'�sht� (9.5)
 
flag *Z: 
    .		>	�'		#  k�rkon > �'k�rkon  (17.5%)
 


# Finally, the suffixes.  These are exactly the suffixes that came out
# with the original "ispell";  I haven't tried to improve them.  The only
# thing I did besides translate them was to add selected cross-product flags.
#
suffixes

#
# Zgjedhimi i par� (foljet me zanore)
# flag B, C, D

flag *B:

    [AE�IOUY] J		>	-J,N		#  lexoj > lexon 

    [AE�IOUY] J		>	-J,JM�		#  lexoj > lexojm�
    [AE�IOUY] J		>	-J,NI		#  lexoj > lexoni
    [AE�IOUY] J		>	-J,JN�		#  lexoj > lexojn�

    [AE�IOUY] J		>	-J,JA		#  lexoj > lexoja
    [AE�IOUY] J		>	-J,JE		#  lexoj > lexoje 
    [AE�IOUY] J		>	-J,NTE		#  lexoj > lexonte

    [AE�IOUY] J		>	-J,NIM		#  lexoj > lexonim
    [AE�IOUY] J		>	-J,NIT		#  lexoj > lexonit
    [AE�IOUY] J		>	-J,NIN		#  lexoj > lexonin

    [AE�IOUY] J		>	-J,SH 		#  lexoj > lexosh
    [AE�IOUY] J		>	-J,J� 		#  lexoj > lexoj�

flag *C:

    [^IUY] [AE�IOUY] J	>	-J,VA		#  lexoj > lexova
    [^IUY] [AE�IOUY] J	>	-J,VE	      	#  lexoj > lexove

    O J			>	-J,I		#  lexoj > lexoi
    O J			>	-OJ,UAM		#  lexoj > lexuam
    O J			>	-OJ,UAN		#  lexoj > lexuan
    O J			>	-OJ,UAT 	#  lexoj > lexuat
    O J			>	-OJ,UAR 	#  lexoj > lexuar

# Diftongjet: ua, ye, ie
 
    U A J		>	-UAJ,OVA	#  shkruaj > shkrova
    U A J		>	-UAJ,OVE	#  shkruaj > shkrove
    U A J		>	-UAJ,OI		#  shkruaj > shkroi

    U A J		>	-UAJ,UAM	#  shkruaj > shkruam
    U A J		>	-UAJ,UAN	#  shkruaj > shkruan
    U A J		>	-UAJ,UAT	#  shkruaj > shkruat

    U A J		>	-UAJ,UAR	#  shkruaj > shkruar

    Y E J		>	-YEJ,EVA	#  lyej > leva
    Y E J		>	-YEJ,EVE	#  lyej > leve
    Y E J		>	-YEJ,EU		#  lyej > leu

    Y E J		>	-YEJ,YEM	#  lyej > lyem
    Y E J		>	-YEJ,YEN	#  lyej > lyen
    Y E J		>	-YEJ,YET	#  lyej > lyet

    Y E J		>	-YEJ,YER	#  lyej > lyer

    I E J		>	-IEJ,JEVA	#  p�rziej > p�rzjeva
    I E J		>	-IEJ,JEVE	#  p�rziej > p�rzjeve
    I E J		>	-IEJ,JEU	#  p�rziej > p�rzjeu

    I E J		>	-IEJ,IEM	#  p�rziej > p�rziem
    I E J		>	-IEJ,IEN	#  p�rziej > p�rzien
    I E J		>	-IEJ,IET	#  p�rziej > p�rziet

    I E J		>	-IEJ,IER	#  p�rziej > p�rzier 

    
flag *D:

# M�nyra d�shirore, koha e tashme

    [^U] [AE�IOUY] J	>	-J,FSHA 	#  lexoj > lexofsha
    [^U] [AE�IOUY] J	>	-J,FSH		#  lexoj > lexofsh
    [^U] [AE�IOUY] J	>	-J,FT�		#  lexoj > lexoft�

    [^U] [AE�IOUY] J	>	-J,FSHIM 	#  lexoj > lexofshim
    [^U] [AE�IOUY] J	>	-J,FSHI 	#  lexoj > lexofshi
    [^U] [AE�IOUY] J	>	-J,FSHIN 	#  lexoj > lexofshin

    U A J		>	-UAJ,OFSHA 	#  shkruaj > shkrofsha
    U A J		>	-UAJ,OFSH 	#  shkruaj > shkrofsh
    U A J		>	-UAJ,OFT�	#  shkruaj > shkroft�

    U A J		>	-UAJ,OFSHIM 	#  shkruaj > shkrofshim
    U A J		>	-UAJ,OFSHI 	#  shkruaj > shkrofshi
    U A J		>	-UAJ,OFSHIN 	#  shkruaj > shkrofshin

# M�nyra habitore, koha e tashme

    O J	 		>	-OJ,UAKAM 	#  lexoj > lexuakam
    O J	  		>	-OJ,UAKE 	#  lexoj > lexuake
    O J			>	-OJ,UAKA 	#  lexoj > lexuaka

    O J			>	-OJ,UAKEMI 	#  lexoj > lexuakemi
    O J			>	-OJ,UAKENI 	#  lexoj > lexuakeni
    O J			>	-OJ,UAKAN 	#  lexoj > lexuakan

    [AE�IUY] J		>	-J,KAM 		#  laj > lakam
    [AE�IUY] J		>	-J,KE 		#  laj > lake
    [AE�IUY] J		>	-J,KA 		#  laj > laka

    [AE�IUY] J		>	-J,KEMI 	#  laj > lakemi
    [AE�IUY] J		>	-J,KENI 	#  laj > lakeni
    [AE�IUY] J		>	-J,KAN 		#  laj > lakan

# Diateza p�sore-vetvetore, koh�t e ndryshme. 
flag *F:

    [AE�IOUY] J	>	-J,HEJ 		#  arg�tohej
    [AE�IOUY] J	>	-J,HEM		#  arg�tohem
    [AE�IOUY] J	>	-J,HEMI		#  arg�tohemi
    [AE�IOUY] J	>	-J,HEN		#  arg�tohen
    [AE�IOUY] J	>	-J,HENI 	#  arg�toheni
    [AE�IOUY] J	>	-J,HESH		#  arg�tohesh
    [AE�IOUY] J	>	-J,HESHA	#  arg�tohesha
    [AE�IOUY] J	>	-J,HESHE	#  arg�toheshe
    [AE�IOUY] J	>	-J,HESHIM	#  arg�toheshim
    [AE�IOUY] J	>	-J,HESHIN	#  arg�toheshin
    [AE�IOUY] J	>	-J,HESHIT	#  arg�toheshit
    [AE�IOUY] J	>	-J,HET		#  arg�tohet
    [AE�IOUY] J	>	-J,HUNI		#  arg�tohuni
 

#
# Zgjedhimi i dyt� (foljet me bashk�ting�llore)
# flag G, H, J

flag *G:	
    [^AE�IOUY]	>	IM		#  hap > hapim 
    [^AE�IOUY]	>	NI		#  hap > hapni
    [^AE�IOUY]	>	IN		#  hap > hapin

    [^AE�IOUY]	>	NIM		#  hap > hapnim
    [^AE�IOUY]	>	NIT		#  hap > hapnit
    [^AE�IOUY]	>	NIN		#  hap > hapnin
    [^AE�IOUY]	>	JA		#  hap > hapja
    [^AE�IOUY]	>	JE		#  hap > hapje
    [^AE�IOUYT]	>	TE		#  hap > hapte
    T 		>       -T, STE         #  mat > maste
    T    	>	TE              #  mat > matte

    [^AE�IOUY]	>	E		#  hap > hape
    [^AE�IOUY]	>	A		#  hap > hapa
    [^AE�IOUY]	>	I		#  hap > hapi
    [^AE�IOUY]	>	UR		#  hap > hapur, mat > matur
    [^AE�IOUY]	>	�		#  hap > hap�
    [^AE�IOUY]	>	�SH		#  hap > hap�sh

flag *H:

# M�nyra d�shirore, koha e tashme

    [^AE�IOUY]	>	SHA		#  hap > hapsha
    [^AE�IOUY]	>	SH		#  hap > hapsh
    [^AE�IOUY]	>	T�		#  hap > hapt�

    [^AE�IOUY]	>	SHIM		#  hap > hapshim
    [^AE�IOUY]	>	SHI		#  hap > hapshi
    [^AE�IOUY]	>	SHIN		#  hap > hapshin

# M�nyra habitore, koha e tashme

    [^AE�IOUY]	>	KAM		#  hap > hapkam
    [^AE�IOUY]	>	KE		#  hap > hapke
    [^AE�IOUY]	>	KA		#  hap > hapka

    [^AE�IOUY]	>	KEMI		#  hap > hapkemi
    [^AE�IOUY]	>	KENI		#  hap > hapkeni
    [^AE�IOUY]	>	KAN		#  hap > hapkan

#flag *J:
# I rezervuar

#
# Lakimi i par�
# flag K L M N

flag *K:

    .	>	I		#  puntor > puntori
    .	>	IN		#  puntor > puntorin
    .   >	IT		#  puntor > puntorit

flag *L:

    [^AE�IOUY]	>	A		#  lis > lisa 
    [^AE�IOUY]	>	ASH		#  lis > lisash 
    [^AE�IOUY]	>	AT		#  lis > lisat
    [^AE�IOUY]	>	AVE		#  lis > lisave

flag *M:
				 
    [^AE�IOUY]	>	�		#  puntor > puntor�
    [^AE�IOUY]	>	�SH		#  puntor > puntor�sh
    [^AE�IOUY]	>	�T		#  puntor > puntor�t
    [^AE�IOUY]	>	�VE		#  puntor > puntor�ve
    

#flag *N:
#				 
#    [AE�] S	>	ISH		#  b�r�s > b�rsish 
#    [AE�] S	>	VE		#  b�r�s > b�r�sve

flag *N:

    [^AE�IOUY]	>	E		#  puntor > puntore
    [^AE�IOUY]	>	ESH		#  puntor > puntoresh
    [^AE�IOUY]	>	ET		#  puntor > puntoret
    [^AE�IOUY]	>	EVE		#  puntor > puntoreve
    
#
# Lakimi i dyt�
# flag: P

flag *P:
    .		>	U		#  dhe > dheu , plak > plaku
    .		>	UT		#  dhe > dheut, plak > plakut	
    .		>	UN		#  dhe > dheun, plak > plakun	

#
# Lakimi i trete
# flag: Q, R, S, T

flag *Q:

    �		>	-�,A		#  shtyll� > shtylla
    �		>	-�,ASH		#  shtyll� > shtyllash
    �		>	-�,AT		#  shtyll� > shtyllat
    �		>	-�,AVE		#  shtyll� > shtyllave
    �		>	-�,E		#  shtyll� > shtylle
    �		>	-�,�N		#  shtyll� > shtyll�n
    �		>	-�,�S		#  shtyll� > shtyll�s

    � R		>	-�R,RA		#  let�r > letra
    � R		>	-�R,RASH	#  let�r > letrash 
    � R		>	-�R,RAT		#  let�r > letrat 
    � R		>	-�R,RAVE	#  let�r > letrave 
    � R		>	-�R,RE		#  let�r > letre
    � R		>	-�R,R�N		#  let�r > letr�n 
    � R		>	-�R,R�S		#  let�r > letr�s

    � R	R	>	-�RR,RRA	#  �nderr > �nderra
    � R	R	>	-�RR,RRASH	#  �nderr > �nderrash 
    � R	R	>	-�RR,RRAT	#  �nderr > �nderrat 
    � R	R	>	-�RR,RRAVE	#  �nderr > �nderrave 
    � R	R	>	-�RR,RRE	#  �nderr > �nderre
    � R	R	>	-�RR,RR�N	#  �nderr > �nderr�n 
    � R	R	>	-�RR,RR�S	#  �nderr > �nderr�s


    � L		>	-�L,LA		#  pik�l > pikla
    � L		>	-�L,LASH	#  pik�l > piklash 
    � L		>	-�L,LAT		#  pik�l > piklat 
    � L		>	-�L,LAVE	#  pik�l > piklave 
    � L		>	-�L,LE		#  pik�l > pikle
    � L		>	-�L,L�N		#  pik�l > pikl�n 
    � L		>	-�L,L�S		#  pik�l > pikl�s

flag *R:

    �		>	-�,A		#  lug� > luga
    �		>	 SH		#  lug� > lug�sh
    �		>	 T		#  lug� > lug�t
    �		>	 VE		#  lug� > lug�ve
    �		>	-�,E		#  lug� > luge
    �		>	 N		#  lug� > lug�n
    �		>	 S		#  lug� > lug�s

flag *S:

    [^JI] E	>	-E,JA		#  lule > lulja
    [JI] E	>	-E,A		#  anije > anija, kund�rth�nie > kund�rth�nia
    O   	>	JA		#  logo > logoja
    [EO]	>	JE		#  lule > luleje
    [EO]	>	N		#  lule > lulen
    [EO]	>	S		#  lule > lules
    [EO]	>	SH		#  lule > lulesh
    [EO]	>	T		#  lule > lulet
    [EO]	>	VE		#  lule > luleve

flag *T:

    [AE�OUY]	>	JA		#  rrufe > rrufeja
    [AE�OUY]	>	JE		#  rrufe > rrufeje
    I		>	A		#  liri > liria
    I		>	E		#  liri > lirie
    [AE�IOUY]	>	N�		#  rrufe > rrufen�, liri > lirin�
    [AE�IOUY]	>	S�		#  rrufe > rrufes�, liri > liris�
    [AE�IOUY]	>	SH		#  rrufe > rrufesh, liri > lirish
    [AE�IOUY]	>	T�		#  rrufe > rrufet�, liri > lirit� 
    [AE�IOUY]	>	VE		#  rrufe > rrufeve, liri > lirive
