#installdir=/usr/lib/ispell

# Gjetja automatike e direkteriumit "LIBDIR"
installdir = $(shell ispell -vv | grep LIBDIR | cut -d '"' -f 2)

all: albanian.hash

install: albanian.hash albanian.aff
	install -o root -g root -m 0644 albanian.hash $(installdir)
	install -o root -g root -m 0644 albanian.aff $(installdir)

albanian.hash: albanian.aff albanian.words+
	buildhash albanian.words+ albanian.aff albanian.hash

clean:
	rm -f albanian.stat albanian.hash albanian.words+.cnt *~


dist:	clean
	tar cvf - * | gzip -9 > ispell-sq-1.6.4.tgz
